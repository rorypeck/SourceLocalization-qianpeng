function varargout= beapcolor(x, y ,z ,Plottitle, xlab, ylab,col,clim,fts,axre)
% 该程序用来使pcolor更好看
% 输入参数说明：
% x为横坐标，y为纵坐标，z为函数值
% Plotitle为标题，xlab为横坐标标签，ylab为纵坐标标签
% col为伪彩图颜色，clim控制colorbar数值范围
% if nargout == 0
%     pcolor(x,y,z);
% else if nargout ==1
h = pcolor(x,y,z);
%     end
% end
    %对图形进行优化
if nargin == 9
    fontsize = fts;
else
    fontsize = 14;
end
colormap(col);
xlabel(xlab,'fontname','黑体','Color','k','FontSize',fontsize-2);
ylabel(ylab,'fontname','黑体','Color','k','FontSize',fontsize-2);
set(gca,'fontsize',fontsize-2)
title( Plottitle ,'fontsize',fontsize,'fontname','黑体')
set(h, 'LineStyle','none');
set(gca, 'YDir', 'reverse');
if nargin == 10
set(gca, 'YDir', 'normal');
end

colorbar;
if nargin >=8 
    caxis(clim);
end
if ( nargout == 1 )
   varargout( 1 ) = { h };   % return a handle to the figure
end
end