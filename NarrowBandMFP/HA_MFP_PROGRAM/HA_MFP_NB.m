clc;  close all;
%阵列水平线阵的窄带匹配场算法
%该程序为匹配场的波束响应程序，并计算模糊度图，阵列为垂直阵
M = 9; rr = linspace(40,-40,M); rd =40; f= 1000; %阵元个数，阵元座标，声源频率
sr = 6000; sd = 25;% 声源位置(距离，深度)
filename = 'pekerisK';%环境文件名
kraken(filename);
[ ~, ~, ~, ~, pos, pressure ] = read_shd([filename '.shd'],f);
pressure = squeeze(pressure);
r= pos.r.range; r = r(floor(M/2)+1:length(r)-floor(M/2)); z = pos.r.z;%扫描声源的位置
index_sr = find(r == sr); index_sd = find(z == sd);%找到声源所在索引
pres = zeros(M, length(z), length(r));%对pres矩阵进行重构，得到水平阵接收矩阵
%这种重构方法必须满足
%1.阵元的距离和声源扫描距离间隔相同
%2.声源起始距离需减去第一个阵元水平位置
for nrr = 1: M
    pres(nrr,:,:) = pressure(:, nrr:nrr-1+length(r));
end
p_new = reshape(pres, M, []);%将矩阵改为2维
p_s = squeeze(pres(:, index_sd, index_sr));%找到目标声源位置所对应导向向量
%模糊度图
p_r = p_new./ (ones(M,1)* sqrt(sum(abs(p_new).^2,1)));%归一化拷贝场
p = p_s/sqrt(p_s'* p_s); %归一化接收场
Azi =reshape(abs(p'* p_r),length(z),[]) ;%模糊度图
Azi_dB = 20*log10(Azi);
figure; set(gcf, 'position', [100 100 900 300]); ax1 = subplot(121)
beapcolor(r/1000,z,Azi_dB, '(a)模糊度图','r/(km)','z/(m)','jet',[-20 0]);
a=max(max(Azi_dB));
[b,c]=find(Azi_dB==a);
fprintf('匹配场输出声源位置为(%d km,%d m)\n',r(c)/1000,z(b))
if index_sr==c&&index_sd == b
    fprintf('匹配场输出位置与真实声源位置一致\n');
else
    fprintf('匹配场输出位置与真实声源位置不一致\n');
end

%计算水平阵简正波分离矩阵
index_rd = find(rd == z);
[ Modes ] = read_modes( [filename,'.mod'], f);
phi = Modes.phi;
k = Modes.k; NM = length(k);
k_v = reshape(k* ones(1,NM)- (k* ones(1,NM)).',[],1);
ind = - k_v * (rr- rr(1));
app = reshape(sum(exp(1j * ind),2), NM,[]);
THETA = phi(index_rd,:).'* phi(index_rd,:).* app;
ax2 = subplot(122)
beapcolor(1:size(phi,2),1:size(phi,2),abs(THETA),'(b)简正波分离矩阵图','简正波号数','简正波号数','parula',[0 1]);
colormap(ax1, jet)
colormap(ax2, parula)

%简正波特征向量
hf = figure; set(hf,'units','normalized','position',[0.1 0.3 0.8 0.6]); nm = 10:15;
for n =1: length(nm)
    subplot(['1' num2str(length(nm)) num2str(n)])
    beaplot(phi(:,nm(n)), z, 'k-',1.5,'',['n=' num2str(nm(n))],'特征向量',1,0)
end