function plot_cpTL(filename,N)
filename=[filename '.TL'];               %文件名
delimiterIn = ' ';                      %列分隔符
headerlinesIn = 3;                      %读取从第 headerlinesIn+1 行开始的数值数据
a=importdata(filename,delimiterIn,headerlinesIn);
depth=str2num(a.textdata{3});
TL=a.data;
NDEP=size(TL,2);
figure(1);
color='mycgbwk';
plot(TL(:,1),TL(:,1+N),color(N));
% plot(TL(:,1),TL(:,1+N),'g');
title('COUPLE计算得到的传播损失');
xlabel('距离/km');
ylabel('传播损失/dB');
hold on
set(gca,'YDir','reverse');
legend([num2str(depth(N)) '米'],'Location','southwest');
end