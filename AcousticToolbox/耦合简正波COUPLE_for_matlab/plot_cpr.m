function plot_cpr(filename)
[Title,freq,pos,R,env,pressure]  = read_cpr(filename);
% [Title,freq,pos,R,env,pressure]  = read_cpr('test');
 zt = pos{6};
 rt = R';
 xlab= 'Range (km)';
tlt = double( abs( pressure ) );   % pcolor needs 'double' because field.m produces a single precision
tlt( isnan( tlt ) ) = 1e-6;   % remove NaNs
tlt( isinf( tlt ) ) = 1e-6;   % remove infinities
icount = find( tlt > 1e-37 );        % for stats, only these values count
tlt( tlt < 1e-37 ) = 1e-37;          % remove zeros
tlt = -20.0 * log10( 4 * pi *tlt ) ;          % so there's no error when we take the log
% compute some statistics to automatically set the color bar
tlmed = median( tlt( icount ) );    % median value
tlstd = std( tlt( icount ) );       % standard deviation
tlmax = tlmed + 0.75 * tlstd;       % max for colorbar
tlmax = 10 * round( tlmax / 10 );   % make sure the limits are round numbers
tlmin = tlmax - 50;                 % min for colorbar
  % 'jet' colormap reversed
figure;
tej = flipud( jet( 256 ) );
h=pcolor( rt,zt,tlt'); ...
shading flat; colormap( tej );caxisrev( [ tlmin, tlmax ] );
xlabel( 'Range (km)' );
ylabel( 'Depth (m)' );
set(gca,'YDir','reverse');
title( deblank(Title ) );
end