function  [Title,freq,pos,R,env,pressure]  = read_cpr(filename)
% % %读取HEADER文件
% filename='test2';
fid = fopen([filename,'.CPR'],'rb');
a=importdata([filename,'.dat'],' ',5);
ss=regexp(a{5}, '\s+', 'split');
s1=ss{1};s2=ss{2};s3=ss{3};
Rmin=str2double(s1); RMAX=str2double(s2);
Rinc=str2double(s3);
RND=(RMAX-Rmin)/Rinc;
PlotTitle=cell(8,1);
for i=1:8
PlotTitle{i}= fread(fid, 10,'*char');
end
%PlotTitle=fread(fid,80,'*char');
date=fread(fid,10,'*char');
env(1:2) = fread(fid, 2, 'float');
env(3:4)=fread(fid,2,'int32');
ndep=env(4);
env(5:4+ndep) = fread(fid, ndep, 'float32');
information= { 'freq','zs','NR','NDEP'};
for i = 1:4
    eval([information{i},'=',num2str(env(i)),';']);
end
ZR=env(5:end);
pos={freq,zs,NR,NDEP,date,ZR};
Title='';
for i=1:8
   u=PlotTitle{i};
   u=u';
   Title=[Title,u];
end
%读取声压值
pr = zeros(RND,NDEP);
pim = zeros(RND,NDEP);
R=zeros(RND,1);
for i = 1: RND
        R(i)=fread(fid,1,'float32');
    for j=1:NDEP
      pr(i,j) = fread(fid, 1, 'float32');
      pim(i,j) = fread(fid, 1, 'float32');
    end
end
pressure = pr+1j*pim;%为了使ram和kraken保持一致，也许这里需要用-1j
feof(fid);
fclose(fid);
end