function couple( filename )

% run the COUPLE program
%
% usage: COUPLE( filename )


runkraken = which( 'COUPLE_1.exe' );
 
if ( isempty( runkraken ) )
   error( 'COUPLE_1.exe not found in your Matlab path' )
else
   eval( [ '! "' runkraken '" ' filename ] );
end