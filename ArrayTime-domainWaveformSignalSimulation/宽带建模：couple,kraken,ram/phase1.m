%% 初始化
close all;clear;clc;

%% 导入数据
load('pingtang.mat'); 
fs=1000;TA=1;  %接收信号的时间
nn=TA*fs+1;%fft点数
cmax=1500;t=0:TA/nn:TA*(1-1/nn);%n点时间
f=0:fs/nn:fs*(1-1/nn);%全频率
delta_f=fs/nn;
% u1_angle=-angle(u1);
uk_phase=angle(u_kraken);
uc_phase=-angle(u_couple);
uc_half=uc_phase;
uk_half=uk_phase;
uc_phase(:,:,1:num1)=[];uc_phase(:,:,end-424:end)=[];
uk_phase(:,:,1:num1)=[];uk_phase(:,:,end-424:end)=[];
%% 相位解卷绕
[uc_phase_re]=phase_re(uc_phase,delta_f,2,cmax);
[uk_phase_re]=phase_re(uk_phase,delta_f,1,cmax);

%% 输出重构相位
close all;
r=3500;z=40;
rr=(r^2+(z-25)^2)^0.5;
delta_t=r/cmax;
uc_re=uc_phase_re(r/10,z/10,:);
uk_re=uk_phase_re(z/10+1,r/10,:);
uc_re=uc_re(:); 
uk_re=uk_re(:);
kc=polyfit(f1',uc_re,1);
kk=polyfit(f1',uk_re,1);
figure(1);
subplot(211);
plot(f1,kc(1)*f1+kc(2));
hold on;
plot(f1,uk_re,'g');
delta_tk=-kk(1)/2/pi;
rk_pre=delta_tk*cmax;
xlabel('频率/Hz');
ylabel('相位/pi');
title('KRAKEN线性拟合后的相位曲线');
fprintf('KRAKEN计算群时延delta_t=%.4f，实际群时延为%.4f,误差为%d\n拟合距离为 %.4f,实际距离为%.4f\n',delta_tk,delta_t,...
abs(delta_tk-delta_t)/delta_t,rk_pre,rr);
figure(2);
subplot(211);
plot(f1,kc(1)*f1+kc(2));
hold on;
plot(f1,uc_re,'g');
delta_tc=-kc(1)/2/pi;
rc_pre=delta_tc*cmax;
xlabel('频率/Hz');
ylabel('相位/pi');
title('COUPLE线性拟合后的相位曲线');
fprintf('COUPLE计算群时延delta_t=%.4f，实际群时延为%.4f,误差为%d\n拟合距离为 %.4f,实际距离为%.4f\n',delta_tc,delta_t,...
abs(delta_tc-delta_t)/delta_t,rc_pre,rr);
%% 函数
function [u1_angle_re]=phase_re(u1_angle,delta_f,mode,cmax)
u1_angle_re=zeros(size(u1_angle));
if mode==1
    %kraken
    for m=1:size(u1_angle,1)
        for n=1:size(u1_angle,2)
            rr=((n*10)^2+(10*m+10-25)^2)^0.5;
            n1=floor(rr/cmax*delta_f);
            for i=1:size(u1_angle,3)-1
                if u1_angle(m,n,i+1)<=u1_angle(m,n,i)
                    u1_angle_re(m,n,i+1)=u1_angle_re(m,n,i)-n1*2*pi-u1_angle(m,n,i)+u1_angle(m,n,i+1);
                    %                 
                elseif u1_angle(m,n,i+1)>u1_angle(m,n,i)
                    u1_angle_re(m,n,i+1)=u1_angle_re(m,n,i)-n1*2*pi-u1_angle(m,n,i)+u1_angle(m,n,i+1)-2*pi;
                    %                 ;
                end
            end
        end
    end
elseif mode==2
    %for couple
    for m=1:size(u1_angle,1)
        for n=1:size(u1_angle,2)
            rr=((m*10)^2+(2*n-25)^2)^0.5;
            n1=floor(rr/cmax*delta_f);
            for i=1:size(u1_angle,3)-1
                if u1_angle(m,n,i+1)<=u1_angle(m,n,i)
                    u1_angle_re(m,n,i+1)=u1_angle_re(m,n,i)-n1*2*pi-u1_angle(m,n,i)+u1_angle(m,n,i+1);
                elseif u1_angle(m,n,i+1)>u1_angle(m,n,i)
                    u1_angle_re(m,n,i+1)=u1_angle_re(m,n,i)-n1*2*pi-u1_angle(m,n,i)+u1_angle(m,n,i+1)-2*pi;               
                end
            end
        end
    end
end
end

