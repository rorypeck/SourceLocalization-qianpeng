function  plot_freq_res(f,u_all)
figure(3);
subplot(2,2,1);
plot(f,abs(u_all));
title('系统幅度响应函数');
xlabel('频率/Hz');
ylabel('幅度响应');
subplot(2,2,2);
plot(f,angle(u_all));
title('频率相位响应函数');
xlabel('频率/Hz');
ylabel('相频特性');
end