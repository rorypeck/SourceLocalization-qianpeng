
function [u]=ram_t(f,FIL)
 t1=clock;
 N=length(f);
 filename=FIL(1:end-3);
 ram_p(filename);filename1=[filename,'_cal.in'];
 filename2=filename1(1:end-3);
 [~,~,~,o]=read_grid(filename);
 u=zeros(size(o,1),size(o,2),N);
for j=1:length(f)
  fid_r=fopen(FIL,'rt');
  fid_w=fopen(filename1,'wt');
  i=0;
  while(feof(fid_r) ~= 1)
    i=i+1;
    line = fgetl(fid_r); 
    if i==2
     fprintf(fid_w,"%s\n",[num2str(f(j)),'  25 30.0      freq zs zr']);
    else
    fprintf(fid_w,"%s\n",line);
    end
  end
  fclose(fid_w);
  fclose(fid_r);
  ram_p(filename2);
  [~,~,~,u(:,:,j)]=read_grid(filename2);
  if j>1
     p1=u(:,:,j)==zeros(size(o,1),size(o,2),1);%是否非零
     p2=u(:,:,j)==u(:,:,j-1);%是否更新
     if ~isempty(find(p1==0, 1)) && ~isempty(find(p2==0, 1))
        fprintf('RAM第%d次运算完成\n',j); 
     elseif isempty(find(p1==0, 1))
        fprintf('RAM第%d次运算失败\n,不收敛',j);  
     elseif isempty(find(p2==0, 1))
         fprintf('RAM第%d次运算失败\n,矩阵没有更新',j);  
     end
  end
end
t2=clock;
delta_t=t2-t1;
fprintf('RAM频率响应计算用时%d时%d分%d秒',delta_t(4),delta_t(5),delta_t(6));
end

