
function [u]=kraken_t(f,FIL)
t1=clock;
N=length(f);
s=FIL;s(end-3:end)='';s1=[s,'_cal'];
kraken(s);
[~,~,~,~,~,I]=read_shd(s);
a=size(I,3);b=size(I,4);
u=zeros(a,b,N);
for j=1:length(f)
  fid_r=fopen(FIL,'rt');
  fid_w=fopen([s1 '.env'],'wt');
  i=0;
  while(feof(fid_r) ~= 1)
    i=i+1;
    line = fgetl(fid_r); 
    if i==2
       fprintf(fid_w,"%s\n",num2str(f(j)));
    else
       fprintf(fid_w,"%s\n",line);
    end
  end
  fclose(fid_w);
  fclose(fid_r);
  kraken(s1);
  [~,~,~,~,~,value]=read_shd(s1);
  u(:,:,j)=reshape(value,a,b,1);
  p1=u(:,:,j)==zeros(a,b,1);%是否非零
  %p2=u(:,:,j)==u(:,:,j-1);%是否更新
  p2=u(1,1,j)==u(1,10,j);
     if ~isempty(find(p1==0, 1)) && ~isempty(find(p2==0, 1))
        fprintf('KRAKEN第%d次运算完成\n',j); 
     elseif isempty(find(p1==0, 1))
        fprintf('KRAKEN第%d次运算失败\n,不收敛',j);  
     elseif isempty(find(p2==0, 1))
         fprintf('KRAKEN第%d次运算失败\n,矩阵问题',j);  
  end
end
t2=clock;
delta_t=t2-t1;
fprintf('KRAKEN频率响应计算用时%d时%d分%d秒',delta_t(4),delta_t(5),delta_t(6));
end
