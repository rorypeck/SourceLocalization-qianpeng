function [Fw_st,F1_st]=freq_res(n,F1,Fw_s,u_all)
Fw_st=zeros(n,1);
F1_st=zeros(n,1);
u_j=u_all;
u_j(n/2+0.5:end)=0;
for i=1:n
    F1_st(i)=F1(i)*u_all(i);
    Fw_st(i)=Fw_s(i)*u_j(i);%与系统函数频域相乘
end
Fw_st(1:10,1)=0;
F1_st(1:10,1)=0;
end