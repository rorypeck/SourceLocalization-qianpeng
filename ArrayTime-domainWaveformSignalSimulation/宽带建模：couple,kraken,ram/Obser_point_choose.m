function [u_s,max_v]=Obser_point_choose(r_1,z_1,u,f1,dz,dr,r0)
m1=round((r_1-r0)/dr);
n1=round(z_1/dz);
if size(u,1)>size(u,2)
    a=m1;m1=n1;n1=a;
end
u_j=u(n1,m1,:);
u_isnan=isnan(u_j);
[max_v,loction]=max(abs(u_j));
u_j(1,1,loction)=0;
for i=1:length(f1)
    if u_isnan(i)==1
      u_j(i)=0;
    end
end
u_s=reshape(u_j,1,size(u_j,3));
end