%此程序用于基于RAM，KRAKEN，COUPLE模型的时域波形预报，其中RAM模型可以处理距离有关边界，三种模型均适用于中低频<1000Hz
%第一部分初始化信号，设置采样频率，以及观察窗时间长度
%第二部分运行模型计算频率响应，使用的相对路径，需要把matlab路径修改至环境文件文件夹路径
%第三部分选择接收点，计算时域波形，并于原信号对比
%需先安装有声学工具包以及fortran语言编译器，并把karken.m ram_p.m添加到系统路径中
%% 初始化
close all;clear;clc;
%% 初始化信号
fs=1000;
fc=50;
T=8/fc;
TA=1;
n=TA*fs+1;
dt=TA/n;  %接收信号的时间
tt=0:dt:T;
s=1/2*sin(2*pi*fc*tt).*(1-cos(1/4*2*pi*fc*tt));%汉宁加权四周期
windows=hamming(length(tt));
ss=s.*windows';
s_hilbert=hilbert(s);
F_hilbert=fft(s_hilbert,n);
F=fft(s,n);
t=0:TA/n:TA*(1-1/n);%n点时间
f=0:fs/n:fs*(1-1/n);%全频率
signal_orginal(f,F_hilbert,F,s_hilbert,t,n);
%% 计算区间内频率点
f_pos=0:fs/n:fs/n*(n-1)/2;%只计算正频率部分
f_range=[25,125];f1=[];num1=0;
for i=1:length(f_pos)    
    if f_pos(i)>=f_range(1)&&f_pos(i)<=f_range(2)
        f1=[f1,f_pos(i)];
    end
    if f_pos(i)<f_range(1)
        num1=num1+1;
    end
end
num2=n/2+0.5-num1-length(f1);
%% 运行水声模型得到频率响应
file_name_kraken='kraken1.env';%选择文件名
file_name_RAM='ram2.in';
file_name_COUPLE='couple0.dat';
mode=5;%选择你要计算的模型,1为RAM，2为kraken,3为COUPLE
if mode==1
    u_ram=ram_t(f1,file_name_RAM);
    u_ram1=cat(3,zeros(size(u_ram,1),size(u_ram,2),num1),u_ram,zeros(size(u_ram,1),size(u_ram,2),num2));
elseif mode==2
    u_kraken=kraken_t(f1,file_name_kraken);
    u_kraken1=cat(3,zeros(size(u_kraken,1),size(u_kraken,2),num1),u_kraken,zeros(size(u_kraken,1),size(u_kraken,2),num2));
elseif mode==3
    u_couple_s=couple_t(f1,'couple11.dat');
    u_couple1_s=cat(3,zeros(size(u_couple_s,1),size(u_couple_s,2),num1),u_couple_s,zeros(size(u_couple_s,1),size(u_couple_s,2),num2));
    u_couple_d=couple_t(f1,'couple0.dat');
    u_couple1_d=cat(3,zeros(size(u_couple_d,1),size(u_couple_d,2),num1),u_couple_d,zeros(size(u_couple_d,1),size(u_couple_d,2),num2));
    u_couple_a=couple_t(f1,'couple2.dat');
    u_couple1_a=cat(3,zeros(size(u_couple_a,1),size(u_couple_a,2),num1),u_couple_a,zeros(size(u_couple_a,1),size(u_couple_a,2),num2));
    u_ram=ram_t(f1,file_name_RAM);
    u_ram1=cat(3,zeros(size(u_ram,1),size(u_ram,2),num1),u_ram,zeros(size(u_ram,1),size(u_ram,2),num2));
elseif mode==4
    u_ram=ram_t(f1,file_name_RAM);
    u_ram1=cat(3,zeros(size(u_ram,1),size(u_ram,2),num1),u_ram,zeros(size(u_ram,1),size(u_ram,2),num2));
    u_kraken=kraken_t(f1,file_name_kraken);
    u_kraken1=cat(3,zeros(size(u_kraken,1),size(u_kraken,2),num1),u_kraken,zeros(size(u_kraken,1),size(u_kraken,2),num2));
    u_couple=couple_t(f1,file_name_COUPLE);
    u_couple1=cat(3,zeros(size(u_couple,1),size(u_couple,2),num1),u_couple,zeros(size(u_couple,1),size(u_couple,2),num2));
elseif mode==5
    u_ram=ram_t(f1,file_name_RAM);
    u_ram1=cat(3,zeros(size(u_ram,1),size(u_ram,2),num1),u_ram,zeros(size(u_ram,1),size(u_ram,2),num2));
    u_couple=couple_t(f1,file_name_COUPLE);
    u_couple1=cat(3,zeros(size(u_couple,1),size(u_couple,2),num1),u_couple,zeros(size(u_couple,1),size(u_couple,2),num2));
end
%% 时域声场预报 频率响应可视化
r_1=20000;%最好取10整数倍，接受点距离
z_1=90;%4整数倍，接受点深度（单位/m）
dz=10;dr=10;%网格间隔，在环境文件中所设置plot(abs(u_s));
[u_s,max_v]=Obser_point_choose(r_1,z_1,u_kraken1,f1,dz,dr,0);
% for i=1:length(u_s)
%     u_s(i)=u_s(i)*exp(1i*2*pi*f_pos(i)*20.05);
% end
RR1 =  conj(fliplr(u_s));RR2=fliplr(u_s);
u_all= [u_s RR1(1,1:length(RR1)-1)];%共轭对称
% [u_all]=Freq_c(u_s);
t_u=ifft(u_all,n);
plot_Hw(u_all,f);
figure(1);
plot(t,t_u);
xlabel('时间/t');
ylabel('声压');
title('冲激响应函数');
Ff=conv(s,t_u);
[Fw_st,F1_st]=freq_res(n,F,F_hilbert,u_all);
freq_signal(f,F1_st,Fw_st)%绘制预报信号频谱
% Fw1=hilbert(Fw_st);
% F11=hilbert(F1_st);
Fw_st=conj(Fw_st);
F1_st=conj(F1_st);
signal_pre1=real(ifft(Fw_st,n));%预报信号值
signal_pre2=real(ifft(F1_st,n));
pre_signal(t,signal_pre1,signal_pre2,s_hilbert,n,r_1,z_1,mode);

function plot_Hw(u_all,f)
   figure(8)
   subplot(2,2,1);
   plot(f,abs(u_all));
   title('系统函数幅度');
   xlabel('频率/Hz');
   ylabel('幅频响应');
   subplot(2,2,2);
   plot(f,angle(u_all));
   title('系统函数相位');
   xlabel('频率/Hz');
   ylabel('相频响应');
end

