%% 初始化
close all;clear;clc;
%% 初始化信号
fs=500;
fmin=70;fmax=100;
SL=180;
dt=1/fs;T=0.2;t=0:dt:T;
pref=1E-6;
Pa=10^(SL/20)*pref;

%s=Pa*sin(2*pi*fmin*t+pi*(fmax-fmin)/T*t.^2);%LFM
s1=Pa*sin(2*pi*fmin*t+pi*(fmax-fmin)/T*t.^2);%LFM
s=hilbert(s1);
TA=2;  %接收信号的时间
n=TA*fs;% n取为偶数！
Fw_s=fft(s,n);
F1=fft(s1,n);
t1=linspace(0,TA-TA/fs,n);
f=linspace(0,2*fs*(1-1/n),n);
signal_orginal(f,Fw_s,F1,s,t1,n);
%% 运行水声模型得到频率响应
f1=linspace(0,fs*(1-2/n),n/2);
file_name='MunkK.env';%选择文件
mode=2;%选择你要计算的模型,1为kraken，2为RAM
if mode==1
   u=ram_t(f1,file_name);
elseif mode==2
   u=kraken_t(f1,file_name);
end
%% 时域声场预报
r_1=1000;%最好取10整数倍，接受点距离
z_1=100;%4整数倍，接受点深度（单位/m）
dz=2;dr=4;%网格间隔，在环境文件中查找
[u_s]=Obser_point_choose(r_1,z_1,u,f1,dz,dr);
[u_all]=Freq_c(u_s);
[Fw_st,F1_st]=freq_res(n,F1,Fw_s,u_all);
freq_signal(f,F1_st,Fw_st)%绘制预报信号频谱
signal_pre1=real(ifft(Fw_st,n));%预报信号值
signal_pre2=real(ifft(F1_st,n));
% t1=linspace(0,TA-TA/fs,n);
% signal1=signal_pre(1:n/2);
% signal2=signal_pre(n/2+1:end);
% signal=[signal2;signal1];
pre_signal(t1,signal_pre1,signal_pre2,s,n,r_1,z_1,mode);