%% 初始化
close all;clear;clc;
%% 初始化信号
fs=500;%采样频率
fmin=70;fmax=100;
SL=180;
dt=1/fs;T=0.2;t=0:dt:T;
pref=1E-6;
Pa=10^(SL/20)*pref;
%s=Pa*sin(2*pi*fmin*t+pi*(fmax-fmin)/T*t.^2);%LFM
s1=Pa*sin(2*pi*fmin*t+pi*(fmax-fmin)/T*t.^2);%LFM
s=hilbert(s1);%希尔伯特变换，取单片谱分析
TA=4;  %接收信号的时间
n=TA*fs;% n取为偶数！
Fw_s=fft(s,n);
F1=fft(s1,n);
f=linspace(0,2*fs*(1-1/n),n);%可视化
t1=linspace(0,TA-TA/fs,n);
signal_orginal(f,Fw_s,F1,s,t1,n);
%% 运行kraken得到频率响应
f1=linspace(0,fs*(1-2/n),n/2);%f为所需要计算的频率点的频率响应 
file_name='MunkK.env';%参数设置文件，请更换当前的文件夹，使用相对路径
u=kraken_t(f1,file_name);%引用kraken_t函数，求解频率响应
%% 时域声场预报
r_1=200;%最好取4整数倍，接受点距离
z_1=200;%2整数倍，接受点深度（单位/m）
dz=2;dr=4;
[u_s]=Obser_point_choose(r_1,z_1,u,f1,dz,dr);%返回所选点的单边频率响应
[u_all]=Freq_c(u_s);%共轭对称构造全频率响应
%plot_freq_res(f,u_all);%绘制频率响应曲线
[Fw_st,F1_st]=freq_res(n,F1,Fw_s,u_all);%返回双边单边两种模式计算的信号频谱。F1为双边谱
freq_signal(f,F1_st,Fw_st)%绘制预报信号频谱
signal_pre1=real(ifft(Fw_st,n));%预报信号值
signal_pre2=real(ifft(F1_st,n));
pre_signal(t1,signal_pre1,signal_pre2,s,n,r_1,z_1);% 预报信号与原始信号对比
