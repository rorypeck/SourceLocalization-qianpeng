function [u_all]=Freq_c(u_s)
u1_angle=angle(u_s);
u1_abs=abs(u_s);
u1_abs(1)=[];
u1_abs=[0,fliplr(u1_abs)];
u1_angle(1)=[];
u1_angle=[0,fliplr(u1_angle)];
u_all_abs=[abs(u_s),u1_abs];
 u_all_angle=[angle(u_s),-u1_angle];
 u_all=u_all_abs.*exp(1i*u_all_angle);
end
