function pre_signal(t1,signal_pre1,signal_pre2,s,n,r_1,z_1,mode)
figure(5);
subplot(2,2,1);
plot(t1,signal_pre1);
title([mode,'时域单边处理信号预报值,距离r=' num2str(r_1)  'm,深度z=',num2str(z_1) 'm']);
xlabel('时间/s');
ylabel('幅值/pa');
subplot(2,2,2);
v=zeros(n,1);
v(1:length(real(s)),1)=real(s);
plot(t1,v);
title('原始信号');
xlabel('时间/s');
ylabel('幅值/pa');
subplot(2,2,3);
plot(t1,signal_pre2);
title(['kraken时域双边处理信号预报值,距离r=' num2str(r_1)  'm,深度z=',num2str(z_1) 'm']);
xlabel('时间/s');
ylabel('幅值/pa');
subplot(2,2,4);
v=zeros(n,1);
v(1:length(real(s)),1)=real(s);
plot(t1,v);
title('原始信号');
xlabel('时间/s');
ylabel('幅值/pa');
end
