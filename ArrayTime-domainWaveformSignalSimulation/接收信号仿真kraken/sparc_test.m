clc;
clear all;
close all;
fmin = 25;
fmax = 75;
fs = 8 * fmax;
fst = 0;
N = 2 ^ ceil(log2((fs - fst)/1));
df = (fs - fst) / N;
tmin = 20;
T = 1/ df;
dt = 1/ (fs- fst);
tj = 20 + [0:N-1] * dt;
f0 = fst + (0:N-1) * df;
f1 = f0((f0>=25));
K = N - length(f1);
f1 = f1(f1<=75);
M=9;
pres = zeros(M, N);
for i = 1: length(f1)
    changeenv(f1(i));
    kraken pekerisK
    [ PlotTitle, ~, freqVec, ~, Pos, pressure ] = read_shd('pekerisK.shd',f1(i));
    pres(:,i+K) = squeeze(pressure);
end
pres_t = df *2* real(ifft(pres.*(ones(M,1)* exp(1j*tmin*2*pi*f0)),[],2));
figure;
scale = max(max(abs(pres_t)));
rd = Pos.r.z;
pres_t_nor = pres_t / scale * rd(length(rd)) / length(rd)...
    + rd* ones(1,N);
set(0,'defaultfigurecolor','w')
plot(tj, pres_t_nor,'linewidth',1.5)
axis ij
title( PlotTitle ,'fontsize',14)
set(gca,'fontsize',12,'fontname','times new roman')
xlabel( 'Time (s)' );
ylabel('Array Depth (m)');

% t = 0 :dt : 4/50;
% s = 0.5*sin(2*pi*50*t).*(1-cos(0.25*2*pi*50*t));
% figure;
% plot(t,s,'linewidth',1.5)
% xlim([0 1])
% p_ready = zeros(length(s), N - length(s) +1);
% s_ready = fliplr(s);
% s_p = zeros(M, N - length(s) +1);
% for i = 1: M
%     for k = 1: N -length(s) +1
%         p_ready(:,k) = pres_t(i,k: k+ length(s) -1);
%     end
%     s_p(i,:) = s_ready * p_ready;
% end
% s_p_nor = 6*s_p/(max(max(s_p)))+ [0: 10: 100]'* ones(1,N- length(s) +1);
% figure;
% plot(tj(1: N - length(s) +1), s_p_nor, 'linewidth' ,1.5)
% axis ij
