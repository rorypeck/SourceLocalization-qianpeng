clear; clc; close all;
%% 发射信号
fs = 1500;  %采样频率
f = [94 112 130 148 166 235];  %深源频率
N = length(f);  %频率个数
T = 40;  %时间40s
df = 1/T; 
t = 0:1/fs:T-1/fs;  %时间序列
s = zeros(N,T*fs); 
frr = 0:df:fs-df;  %频率
for i = 1:N
s(i,:) = cos(2*pi*f(1,i)*t);  %时域信号
end
s = sum(s,1);
s_F = fft(s,T*fs,2);  %信号的傅里叶变换

%% 生成声压场
h_suppose = [94.125 99.755 105.38 111.00 116.62 122.25 127.88 ...
    139.12 144.74 150.38 155.99 161.62 167.26 172.88 178.49 ...
    184.12 189.76 195.38 200.99 206.62 212.25];  %阵列深度
M = length(h_suppose);  %阵列数量
% pressure = zeros(20,200,N,M);
% for j = 1:M
%     for i = 1:N
%         change_senSO('SD',f(1,i),h_suppose(1,j),'Munkbty');  %修改环境文件中的频率和深度  
%         ram_p2 Munkbty  %运行RAM模型
%         [PlotTitle,fre, Pos, pressure(:,:,i,j)] = read_grid('Munkbty');  %读取声压
%     end
% end
% save('Pres.mat','pressure')  %存储声压数据
load(['Pres' '.mat']);  %加载声压数据
pressure = permute(pressure,[4,3,1,2]);  %对声场做互易
for i = 1:6
    [~,ifr] = min(abs(f-166));  %找到频率为166Hz的索引
end
pressu(:,:,:) = pressure(:,ifr,:,:);  %提取频率为166Hz的声压
pr_norm = reshape(pressu./sqrt(ones(size(pressu)).*sum(abs(pressu).^2,1)), M, []);  %拷贝场归一化
range = [0.05:0.05:10];  %距离，单位为km
z = [10:10:200];  %深度，单位为m
Sr = 2.5;  %声源距离为2.5km
Sd = 60;  %声源深度为60m
[Srd, Sdr] = meshgrid(Sr, Sd);  %基于声源深度和距离中包含的坐标返回二维网格坐标
for index = 1: numel(Srd)
    [~, isr] = min(abs(range - Srd(index)));  %找寻range中与声源距离的差为最小值的数据的索引
    [~, isd] = min(abs(z - Sdr(index)));  %找寻z中与声源深度的差为最小值的数据的索引
    pres = pressure(:,:,isd,isr);  %提取距离为2.5km，深度为60m的声压
end

%% 计算时域波形
H_all = zeros(21,60000);
for dex = 1:6
    [~, isf] = min(abs(frr - f(1,dex)));
    H_all(:,isf) = pres(:,dex);  %补零
end
y_ram = zeros(21,60000);
for i = 1:21
    y_ram(i,:) = real(ifft(s_F.*H_all(i, :).*exp(-1j*2*pi*frr.*2500/1500),'symmetric'));  %傅里叶逆变换
end

%% 加噪声
snr = 0;
y_ram = awgn(y_ram,snr,'measured');   %信噪比为0

%% 分段处理
K = 0;
ps_norm = 0;
for isnap = 1:28  %28个快照
    x_sn = y_ram(:,1+(isnap-1)*2048:2048+2048*isnap);  %共4096个样本，50%重叠
    w = kaiser(21,2.5);  %每个快照都使用归一化的凯瑟窗，调整参数为2.5
    x_snap = x_sn.*w;  %对时域信号加窗
    Fx_snap = fft(x_snap,4096,2);  %作傅里叶变换
    fk = (0:4096-1)/4096*fs;
    [~,ifreq] = min(abs(fk-166));  %找到166Hz的频率的索引
    flag = 0;
    for i = -1:1
        ifreqi = ifreq+i;  %搜索相邻的±1个频点
        flag = flag+1;
        ps(:,flag) = Fx_snap(:,ifreqi);
    end
    for ii = 1:3
        ps_nor(:,ii) = norm(ps(:,ii));
    end
    [~,imax] = max(ps_nor);  %找到对应最大功率的索引，以适应多普勒频移
    ps_n = ps(:,imax)/norm(ps(:,imax));  %对测量矩阵作归一化
    K = K+ps_n*ps_n';
    ps_norm = ps_n+ps_norm;
end
k = K/28+0.01*eye(M);
ps_norm = ps_norm/28;

%% 匹配场定位
tic;
CMFP = abs(sum((pr_norm'*k).'.*pr_norm));  %线性匹配场定位算法
toc;
tic;
AMFP = abs(1./sum((pr_norm'/k).'.*pr_norm ,1));  %自适应匹配场定位算法
toc;
tic;
CS = omp(ps_norm,pr_norm,4000);  %基于压缩感知的匹配场定位算法
toc;
CMFP_dB = 10*log10(reshape(gather(CMFP),length(z),[]));  %单位转化为dB
AMFP_dB = 10*log10(reshape(gather(AMFP),length(z),[]));  %单位转化为dB
CS_dB = 10*log10(reshape(gather(abs(CS)),length(z),[]));  %单位转化为dB
figure
subplot(3,1,1)
beapcolor(range,z,CMFP_dB,'','r/(km)','z/(m)','parula')
caxis([-15 0])
title('Bartlett MFP');
hold on
plot(2.5,60,'--gs','LineWidth',2,'MarkerSize',20,'MarkerEdgeColor','w')
subplot(3,1,2)
beapcolor(range,z,AMFP_dB,'','r/(km)','z/(m)','parula')
title('MVDR MFP');
hold on
plot(2.5,60,'--gs','LineWidth',2,'MarkerSize',20,'MarkerEdgeColor','w')
subplot(3,1,3)
beapcolor(range,z,CS_dB,'','r/(km)','z/(m)','parula')
title('CS MFP');
hold on
plot(2.5,60,'--gs','LineWidth',2,'MarkerSize',20,'MarkerEdgeColor','w')
[~,C_max_idx] = max(CMFP);
fprintf("CMFP Estimate range is %.1f km\n", range(fix(C_max_idx/ length(z))+1));
fprintf("CMFP Estimate depth is %.1f  m\n", z(mod(C_max_idx, length(z))  ));
[~,A_max_idx] = max(AMFP);
fprintf("AMFP Estimate range is %.1f km\n", range(fix(A_max_idx/ length(z))+1));
fprintf("AMFP Estimate depth is %.1f  m\n", z(mod(A_max_idx, length(z))  ));
[~,CS_max_idx] = max(CS);
fprintf("CS Estimate range is %.1f km\n", range(fix(CS_max_idx/ length(z))+1));
fprintf("CS Estimate depth is %.1f  m\n", z(mod(CS_max_idx, length(z))  ));