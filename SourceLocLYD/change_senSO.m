function change_senSO(type,ff,para,filename)
%该函数用来改变环境文件中的海水深度等参数
%参数说明
%type为参数名
%para为参数值
%filename为环境文件名
%读写env
switch type
    case 'freq'
        fid = fopen([filename,'.in'],'rt+');
        i = 0;
        newline = cell(999,1);
        while ~feof(fid)
            tline = fgetl(fid);
            i = i + 1;
            newline{i} = tline;
        end
        newline{1} = [num2str(para(1),'%4.1f') '  ' num2str(para(end),'%4.1f') '  ' ...
            num2str(para(2)-para(1),'%4.1f    fst  fed  df')];
        fclose(fid);
        fid2 = fopen([filename,'.in'],'wt+');
        for k = 1: i
            fprintf(fid2,'%s \n',newline{k});
        end
        fclose(fid2);
    case 'SD'
        fid = fopen([filename,'.in'],'rt+');
        i = 0;
        newline = cell(999,1);
        while ~feof(fid)
            tline = fgetl(fid);
            i = i + 1;
            newline{i} = tline;
        end
        newline{2} = sprintf('%4.1f   %4.1f   80.0   freq zs zr',ff(1),para(1));
        %newline{2} = num2str(para(1),'230.0   %4.1f   80.0          freq zs zr');
        fclose(fid);
        fid2 = fopen([filename,'.in'],'wt+');
        for k = 1: i
            fprintf(fid2,'%s \n',newline{k});
        end
        fclose(fid2);
end
end