clear; clc; close all;
%% 发射信号
fs = 1500;  %采样频率
f = [94 112 130 148 166 235];  %深源频率
N = length(f);  %频率个数
T = 40;  %时间40s
df = 1/T; 
t = 0:1/fs:T-1/fs;  %时间序列
s = zeros(N,T*fs); 
frr = 0:df:fs-df;  %频率
for i = 1:N
s(i,:) = cos(2*pi*f(1,i)*t);  %时域信号
end
s = sum(s,1);
s_F = fft(s,T*fs,2);  %信号的傅里叶变换

%% 生成声压场
h_suppose = [94.125 99.755 105.38 111.00 116.62 122.25 127.88 ...
    139.12 144.74 150.38 155.99 161.62 167.26 172.88 178.49 ...
    184.12 189.76 195.38 200.99 206.62 212.25];  %阵列深度
M = length(h_suppose);  %阵列数量
% pressure = zeros(20,200,N,M);
% for j = 1:M
%     for i = 1:N
%         change_senSO('SD',f(1,i),h_suppose(1,j),'Munkbty');  %修改环境文件中的频率和深度  
%         ram_p2 Munkbty  %运行ram模型
%         [PlotTitle,fre, Pos, pressure(:,:,i,j)] = read_grid('Munkbty');  %读取声压
%     end
% end
% save('Pres.mat','pressure')  %存储声压数据
load(['Pres' '.mat']);  %加载声压数据
pressure = permute(pressure,[4,3,1,2]);  %对声场做互易
for i = 1:6
    [~,ifr] = min(abs(f-166));  %找到频率为166Hz的索引
end
pressu(:,:,:) = pressure(:,ifr,:,:);  %提取频率为166Hz的声压
pr_norm = reshape(pressu./sqrt(ones(size(pressu)).*sum(abs(pressu).^2,1)), M, []);  %拷贝场归一化
range = [0.05:0.05:10];  %距离，单位为km
z = [10:10:200];  %深度，单位为m
Sr = 2.5;  %第一个声源距离为2.5km
Sd = 60;  %第一个声源深度为60m
[Srd, Sdr] = meshgrid(Sr, Sd);  %基于声源深度和距离中包含的坐标返回二维网格坐标
for index = 1: numel(Srd)
    [~, isr] = min(abs(range - Srd(index)));  %找寻range中与声源距离的差为最小值的数据的索引
    [~, isd] = min(abs(z - Sdr(index)));  %找寻z中与声源深度的差为最小值的数据的索引
    pres1 = pressure(:,:,isd,isr);  %提取距离为2.5km，深度为60m的声压
end
Sr2 = 3.5;  %第二个声源距离为3.5km
Sd2 = 60;  %第二个声源深度为60m
[Srd2, Sdr2] = meshgrid(Sr2, Sd2);  %基于声源深度和距离中包含的坐标返回二维网格坐标
for index = 1: numel(Srd2)
    [~, isr2] = min(abs(range - Srd2(index)));  %找寻range中与声源距离的差为最小值的数据的索引
    [~, isd2] = min(abs(z - Sdr2(index)));  %找寻z中与声源深度的差为最小值的数据的索引
    pres2 = pressure(:,:,isd2,isr2);  %提取距离为3.5km，深度为60m的声压
end

%% 计算时域波形
H_all1 = zeros(21,60000);
for dex = 1: 6
    [~, isf] = min(abs(frr - f(1,dex)));
    H_all1(:,isf) = pres1(:,dex);  %补零
end
y_ram1 = zeros(21,60000);
for i = 1:21
    y_ram1(i,:) = real(ifft(s_F.*H_all1(i, :).*exp(-1j*2*pi*frr.*2500/1500),'symmetric'));  %傅里叶逆变换
end
H_all2 = zeros(21,60000);
for dex = 1: 6
    [~, isf] = min(abs(frr - f(1,dex)));
    H_all2(:,isf) = pres2(:,dex);  %补零
end
y_ram2 = zeros(21,60000);
for i = 1:21
    y_ram2(i,:) = real(ifft(s_F.*H_all2(i, :).*exp(-1j*2*pi*frr.*2500/1500),'symmetric'));  %傅里叶逆变换
end

%% 加噪声
snr1 = 3;
y_ram1 = awgn(y_ram1,snr1,'measured');   %信噪比为3dB
snr2 = 0;
y_ram2 = awgn(y_ram2,snr2,'measured');   %信噪比为0dB
y_ram = y_ram1+y_ram2;  %双源

%% 分段处理
K = 0;
ps_norm = 0;
for isnap = 1:28  %28个快照
    x_sn = y_ram(:,1+(isnap-1)*2048:2048+2048*isnap);  %共4096个样本，50%重叠
    w = kaiser(21,2.5);  %每个快照都使用归一化的凯瑟窗，调整参数为2.5
    x_snap = x_sn.*w;  %对时域信号加窗
    Fx_snap = fft(x_snap,4096,2);  %作傅里叶变换
    fk = (0:4096-1)/4096*fs;
    [~,ifreq] = min(abs(fk-166));  %找到166Hz的频率的索引
    flag = 0;
    for i = -1:1
        ifreqi = ifreq+i;  %搜索相邻的±1个频点
        flag = flag+1;
        ps(:,flag) = Fx_snap(:,ifreqi);
    end
    for ii = 1:3
        ps_nor(:,ii) = norm(ps(:,ii));
    end
    [~,imax] = max(ps_nor);  %找到对应最大功率的索引，以适应多普勒频移
    ps_n = ps(:,imax)/norm(ps(:,imax));  %对测量矩阵作归一化
    K = K+ps_n*ps_n';
    ps_norm = ps_n+ps_norm;
end
k = K/28+0.01*eye(M);
ps_norm = ps_norm/28;

%% 匹配场定位
tic;
CMFP = abs(sum((pr_norm'*k).'.*pr_norm));  %线性匹配场定位算法
toc;
tic;
AMFP = abs(1./sum((pr_norm'/k).'.*pr_norm ,1));  %自适应匹配场定位算法
toc;
tic;
CS = omp(ps_norm,pr_norm,4000);  %基于压缩感知的匹配场定位算法
toc;
CMFP_dB = 10*log10(reshape(gather(CMFP),length(z),[]));  %单位转化为dB
AMFP_dB = 10*log10(reshape(gather(AMFP),length(z),[]));  %单位转化为dB
CS_dB = 10*log10(reshape(gather(abs(CS)),length(z),[]));  %单位转化为dB
figure
subplot(3,1,1)
beapcolor(range,z,CMFP_dB,'','r/(km)','z/(m)','parula')
caxis([-15 0])
title('Bartlett MFP');
hold on
plot(2.5,60,'--gs','LineWidth',2,'MarkerSize',20,'MarkerEdgeColor','w')
hold on
plot(3.5,60,'--gs','LineWidth',2,'MarkerSize',20,'MarkerEdgeColor','w')
subplot(3,1,2)
beapcolor(range,z,AMFP_dB,'','r/(km)','z/(m)','parula')
title('MVDR MFP');
hold on
plot(2.5,60,'--gs','LineWidth',2,'MarkerSize',20,'MarkerEdgeColor','w')
hold on
plot(3.5,60,'--gs','LineWidth',2,'MarkerSize',20,'MarkerEdgeColor','w')
subplot(3,1,3)
beapcolor(range,z,CS_dB,'','r/(km)','z/(m)','parula')
title('CS MFP');
hold on
plot(2.5,60,'--gs','LineWidth',2,'MarkerSize',20,'MarkerEdgeColor','w')
hold on
plot(3.5,60,'--gs','LineWidth',2,'MarkerSize',20,'MarkerEdgeColor','w')