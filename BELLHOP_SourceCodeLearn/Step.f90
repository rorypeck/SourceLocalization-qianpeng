MODULE Step

  USE bellhopMod
  USE sspMod
  IMPLICIT NONE
CONTAINS

  SUBROUTINE Step2D( ray0, ray2, Topx, Topn, Botx, Botn )

    ! Does a single step along the ray
    ! x denotes the ray coordinate, (r,z)
    ! t denotes the scaled tangent to the ray (previously (rho, zeta))
    ! c * t would be the unit tangent

    TYPE( ray2DPt )    :: ray0, ray1, ray2
    REAL (KIND=8 ), INTENT( IN ) :: Topx( 2 ), Topn( 2 ), Botx( 2 ), Botn( 2 )
    INTEGER            :: iSegz0, iSegr0
    REAL     (KIND=8 ) :: gradc0( 2 ), gradc1( 2 ), gradc2( 2 ), &
         c0, cimag0, crr0, crz0, czz0, csq0, cnn0_csq0, &
         c1, cimag1, crr1, crz1, czz1, csq1, cnn1_csq1, &
         c2, cimag2, crr2, crz2, czz2, urayt0( 2 ), urayt1( 2 ), &
         h, halfh, hw0, hw1, ray2n( 2 ), RM, RN, gradcjump( 2 ), cnjump, csjump, w0, w1, rho 

    ! The numerical integrator used here is a version of the polygon (a.k.a. midpoint, leapfrog, or Box method), and similar
    ! to the Heun (second order Runge-Kutta method).
    ! 这里使用的数值积分器是多边形的一个版本（也称为中点，蛙跳，或者箱方法），以及类似的Heun（二阶Runge-Kutta方法）
    ! However, it's modified to allow for a dynamic step change, while preserving the second-order accuracy).
    ! 然而，它被修改为允许动态阶跃变化，同时保证二阶精度
    ! *** Phase 1 (an Euler step)
    ! 阶段1（欧拉阶跃）

    CALL EvaluateSSP( ray0%x, c0, cimag0, gradc0, crr0, crz0, czz0, rho, freq, 'TAB' )

    csq0      = c0 * c0
    cnn0_csq0 = crr0 * ray0%t( 2 )**2 - 2.0 * crz0 * ray0%t( 1 ) * ray0%t( 2 ) + czz0 * ray0%t( 1 )**2
    ! 这个参数是什么意思
    iSegz0    = iSegz     ! make note of current layer
    iSegr0    = iSegr

    h = Beam%deltas       ! initially set the step h, to the basic one, deltas
    urayt0 = c0 * ray0%t  ! unit tangent

    CALL ReduceStep2D( ray0%x, urayt0, iSegz0, iSegr0, Topx, Topn, Botx, Botn, h ) ! reduce h to land on boundary
    ! 当快碰到边界时，减小步长h
    halfh = 0.5 * h   ! first step of the modified polygon method is a half step

    ray1%x = ray0%x + halfh * urayt0
    ray1%t = ray0%t - halfh * gradc0 / csq0
    ray1%p = ray0%p - halfh * cnn0_csq0 * ray0%q
    ray1%q = ray0%q + halfh * c0        * ray0%p

    ! *** Phase 2
    ! 阶段2 

    CALL EvaluateSSP( ray1%x, c1, cimag1, gradc1, crr1, crz1, czz1, rho, freq, 'TAB' )
    csq1      = c1 * c1
    cnn1_csq1 = crr1 * ray1%t( 2 )**2 - 2.0 * crz1 * ray1%t( 1 ) * ray1%t( 2 ) + czz1 * ray1%t( 1 )**2

    ! The Munk test case with a horizontally launched ray caused problems.
    ! The ray vertexes on an interface and can ping-pong around that interface.
    ! Have to be careful in that case about big changes to the stepsize (that invalidate the leap-frog scheme) in phase II.
    ! A modified Heun or Box method could also work.
    ! 带有水平发射射线的Munk测试用例造成了问题。
    ! 声线在一个界面上达到定点，并且可以在该界面周围像乒乓一样弹射。
    ! 在这种情况下，必须小心第一阶段步长的巨大变化（使蛙跳方案无效）
    ! 改良的Heun或Box方法可以起作用。

    urayt1 = c1 * ray1%t   ! unit tangent
    ! 单位方位向量

    CALL ReduceStep2D( ray0%x, urayt1, iSegz0, iSegr0, Topx, Topn, Botx, Botn, h ) ! reduce h to land on boundary

    ! use blend of f' based on proportion of a full step used.
    w1  = h / ( 2.0d0 * halfh )
    ! 这里变为了2倍的halfh，w1 = h / (2.0d0 * halfh)，这里是一个权重？？？
    ! 
    w0  = 1.0d0 - w1
    hw0 = h * w0
    hw1 = h * w1
    ! 分了两步，两步的方向不一样，可能是碰到了边界？

    ray2%x   = ray0%x   + hw0 * urayt0              + hw1 * urayt1
    ray2%t   = ray0%t   - hw0 * gradc0 / csq0       - hw1 * gradc1 / csq1
    ray2%p   = ray0%p   - hw0 * cnn0_csq0 * ray0%q  - hw1 * cnn1_csq1 * ray1%q
    ray2%q   = ray0%q   + hw0 * c0        * ray0%p  + hw1 * c1        * ray1%p
    ray2%tau = ray0%tau + hw0 / CMPLX( c0, cimag0, KIND=8 ) + hw1 / CMPLX( c1, cimag1, KIND=8 )

    ray2%Amp       = ray0%Amp
    ray2%Phase     = ray0%Phase
    ray2%NumTopBnc = ray0%NumTopBnc
    ray2%NumBotBnc = ray0%NumBotBnc

    ! If we crossed an interface, apply jump condition
    ! 如果穿越了边界，应用跳跃条件

    CALL EvaluateSSP( ray2%x, c2, cimag2, gradc2, crr2, crz2, czz2, rho, freq, 'TAB' )
    ray2%c = c2

    IF ( iSegz /= iSegz0 .OR. iSegr /= iSegr0 ) THEN
       gradcjump =  gradc2 - gradc0
       ! 梯度的跳跃
       ray2n     = [ -ray2%t( 2 ), ray2%t( 1 ) ]   ! ray normal
       ! 法向向量

       cnjump    = DOT_PRODUCT( gradcjump, ray2n  )
       ! cnjump声速对法向的偏导？？
       csjump    = DOT_PRODUCT( gradcjump, ray2%t )
       ! csjump声速对声速切向的偏导？？

       IF ( iSegz /= iSegz0 ) THEN         ! crossing in depth
       ! 穿过深度
          RM = +ray2%t( 1 ) / ray2%t( 2 )  ! this is tan( alpha ) where alpha is the angle of incidence
          ! tan(alpha)声线的掠射角
       ELSE                                ! crossing in range
       ! 穿过距离
          RM = -ray2%t( 2 ) / ray2%t( 1 )  ! this is tan( alpha ) where alpha is the angle of incidence
          ! tan(alpha)声线的掠射角，这个方向和穿过深度的方向相反，似乎代表着越过了边界？？
       END IF

       RN     = RM * ( 2 * cnjump - RM * csjump ) / c2
       ray2%p = ray2%p - ray2%q * RN

    END IF

  END SUBROUTINE Step2D

  ! **********************************************************************!

  SUBROUTINE ReduceStep2D( x0, urayt, iSegz0, iSegr0, Topx, Topn, Botx, Botn, h )

    ! calculate a reduced step size, h, that lands on any points where the environment changes
    ! 计算了一个较小的步长h，它落在了环境变换的任何点上

    USE BdryMod
    INTEGER,       INTENT( IN    ) :: iSegz0, iSegr0                             ! SSP layer the ray is in
    REAL (KIND=8), INTENT( IN    ) :: x0( 2 ), urayt( 2 )                        ! ray coordinate and tangent
    REAL (KIND=8), INTENT( IN    ) :: Topx( 2 ), Topn( 2 ), Botx( 2 ), Botn( 2 ) ! Top, bottom coordinate and normal
    REAL (KIND=8), INTENT( INOUT ) :: h                                          ! reduced step size 
    REAL (KIND=8)                  :: x( 2 ), d( 2 ), d0( 2 ), h1, h2, h3, h4, rSeg( 2 )

    ! Detect interface or boundary crossing and reduce step, if necessary, to land on that crossing.
    ! Keep in mind possibility that user put source right on an interface 
      and that multiple events can occur (crossing interface, top, and bottom in a single step).
    ! 检测界面或边界交叉点，并在必要时减少在该交叉点上的着陆的步骤
    ! 记住用户将源代码放在界面上的可能性并且可以发生多个事件（在一个步骤中跨越界面、顶部和底部）。

    x = x0 + h * urayt ! make a trial step
    ! 试着迈出第一步

    ! interface crossing in depth
    ! 界面深度交叉
    h1 = huge( h1 )
    ! 取一个极大的real
    IF ( ABS( urayt( 2 ) ) > EPSILON( h1 ) ) THEN
    ! 判断声线的矢量向量的y是否大于一个极小值？？？ 大于0？？？
       IF      ( SSP%z( iSegz0     ) > x(  2 ) ) THEN
          ! 判断声速的当前深度是否大于当前声线的深度
          h1 = ( SSP%z( iSegz0     ) - x0( 2 ) ) / urayt( 2 )
          ! 如果是，那么步长等于两者的深度差除以urayt(2)，实际上是sin(alpha)，得到下一步的声线长度h1
       ELSE IF ( SSP%z( iSegz0 + 1 ) < x(  2 ) ) THEN
          ! 判断声速的下一个深度是否大于当前声线的深度
          h1 = ( SSP%z( iSegz0 + 1 ) - x0( 2 ) ) / urayt( 2 )
          ! 如果是，那么步长等于两者的深度差除以urayt(2)，实际上是sin(alpha)，得到下一步的声线长度h1 
       END IF
    END IF

    ! top crossing
    ! 穿过顶部
    h2 = huge( h2 )
    d  = x - Topx              ! vector from top to ray
    ! 从顶端到声线的向量
    IF ( DOT_PRODUCT( Topn, d ) > EPSILON( h2 ) ) THEN
    ! DOT_PRODUCT计算得到的结果为顶端到声线在顶端外法向上的距离，如果距离大于0，说明还没有碰到顶端
       d0  = x0 - Topx         ! vector from top    node to ray origin
       ! i计算从顶部的节点到声线起点的向量
       h2 = -DOT_PRODUCT( d0, Topn ) / DOT_PRODUCT( urayt, Topn )
       !  DOT_PRODUCT( urayt, Topn )实际上是两个单位向量的夹角，得到的h2为声线沿着urayt方向到达顶部的距离
    END IF

    ! bottom crossing
    ! 穿过底部
    h3 = huge( h3 )
    d  = x - Botx              ! vector from bottom to ray
    IF ( DOT_PRODUCT( Botn, d ) > EPSILON( h2 ) ) THEN
       d0  = x0 - Botx         ! vector from bottom node to ray origin
       h3 = -DOT_PRODUCT( d0, Botn ) / DOT_PRODUCT( urayt, Botn )
    END IF

    ! top or bottom segment crossing in range
    ! 穿过顶部或者底部的距离，这里我没有看懂，rTopSeg是怎么来的，代表什么含义？？？
    rSeg( 1 ) = MAX( rTopSeg( 1 ), rBotSeg( 1 ) )
    rSeg( 2 ) = MIN( rTopSeg( 2 ), rBotSeg( 2 ) )

    IF ( SSP%Type == 'Q' ) THEN
       rSeg( 1 ) = MAX( rSeg( 1 ), SSP%Seg%r( iSegr0     ) )
       rSeg( 2 ) = MIN( rSeg( 2 ), SSP%Seg%r( iSegr0 + 1 ) )
    END IF

    h4 = huge( h4 )
    IF ( ABS( urayt( 1 ) )  > EPSILON( h4 ) ) THEN
       IF       ( x(  1 ) < rSeg( 1 ) ) THEN
          h4 = -( x0( 1 ) - rSeg( 1 ) ) / urayt( 1 )
       ELSE IF  ( x(  1 ) > rSeg( 2 ) ) THEN
          h4 = -( x0( 1 ) - rSeg( 2 ) ) / urayt( 1 )
       END IF
    END IF

    h = MIN( h, h1, h2, h3, h4 )           ! take limit set by shortest distance to a crossing
    ! 取出最小的距离
    IF ( h < 1.0d-4 * Beam%deltas ) THEN   ! is it taking an infinitesimal step?
    ! 这个距离是否无穷小，如果是
       h = 1.0d-5 * Beam%deltas            ! make sure we make some motion
       ! 确保我们的运动是很小的
       iSmallStepCtr = iSmallStepCtr + 1   ! keep a count of the number of sequential small steps
       ! 记录小运动步的次数
    ELSE
       iSmallStepCtr = 0   ! didn't do a small step so reset the counter
       ! 没有走较小的步，因此初始化参数
    END IF

  END SUBROUTINE ReduceStep2D

END MODULE Step
