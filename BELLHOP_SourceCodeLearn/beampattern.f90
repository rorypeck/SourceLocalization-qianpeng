MODULE beampattern

  ! Loads a source beam pattern
  ! 读取声源指向性的Module

  USE FatalError
  SAVE
  ! 保存子程序每次运行变量的值，下次运行时直接调用
  INTEGER, PARAMETER         :: SBPFile = 50
  INTEGER                    :: NSBPPts          ! Number of source beam-pattern points
  ! .sbp声源指向性的角度数
  REAL (KIND=8), ALLOCATABLE :: SrcBmPat( :, : )
  ! SBP
  CHARACTER (LEN=1)          :: SBPFlag          ! '*' or 'O' to indicate a directional or omni pattern

CONTAINS

  SUBROUTINE ReadPat( FileRoot, PRTFile )
  ! 读取.sbp的函数

    IMPLICIT NONE
    INTEGER,            INTENT( IN ) :: PRTFile  ! Unit for print file
    INTEGER                          :: I, IAllocStat, IOStat
    CHARACTER (LEN=80), INTENT( IN ) :: FileRoot

    IF ( SBPFlag == '*' ) THEN
    ! 这个SBPFlag指的是runtype(3)第三个元素
       WRITE( PRTFile, * )
       WRITE( PRTFile, * ) '______________________________'
       WRITE( PRTFile, * ) 'Using source beam pattern file'

       OPEN( UNIT = SBPFile,   FILE = TRIM( FileRoot ) // '.sbp', STATUS = 'OLD', IOSTAT = IOStat, ACTION = 'READ' )
       IF ( IOstat /= 0 ) THEN
          WRITE( PRTFile, * ) 'SBPFile = ', TRIM( FileRoot ) // '.sbp'
          CALL ERROUT( 'BELLHOP-ReadPat', 'Unable to open source beampattern file' )
       END IF

       READ(  SBPFile, * ) NSBPPts
       WRITE( PRTFile, * ) 'Number of source beam pattern points', NSBPPts

       ALLOCATE( SrcBmPat( NSBPPts, 2 ), Stat = IAllocStat )
       IF ( IAllocStat /= 0 ) &
            CALL ERROUT( 'BELLHOP-ReadPat', 'Insufficient memory for source beam pattern data: reduce # SBP points' )

       WRITE( PRTFile, * )
       WRITE( PRTFile, * ) ' Angle (degrees)  Power (dB)'

       DO I = 1, NSBPPts
          READ(  SBPFile, * ) SrcBmPat( I, : )
          ! SrcBmPat 每行包含两个元素，角度 Angle (degrees) 和 功率 Power (dB)
          WRITE( PRTFile, FMT = "( 2G11.3 )" ) SrcBmPat( I, : )
       END DO

    ELSE   ! no pattern given, use omni source pattern
    ! omni指的是全向声源，即点声源
       NSBPPts = 2
       ALLOCATE( SrcBmPat( 2, 2 ), Stat = IAllocStat )
       IF ( IAllocStat /= 0 ) CALL ERROUT( 'BELLHOP-ReadPat', 'Insufficient memory'  )
       SrcBmPat( 1, : ) = [ -180.0, 0.0 ]
       SrcBmPat( 2, : ) = [  180.0, 0.0 ]
    ENDIF

    SrcBmPat( :, 2 ) = 10 ** ( SrcBmPat( :, 2 ) / 20 )  ! convert dB to linear scale
    ! 这里将对数（dB）转化为了线性尺度，注意这里除了20，因此这里得到的SrcBmPat是声压指向而不是声强指向
    ! 也就是说，是要与声压相乘
  END SUBROUTINE ReadPat

END MODULE beampattern
