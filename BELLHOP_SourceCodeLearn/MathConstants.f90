MODULE MathConstants

   SAVE
   REAL    (KIND=8), PARAMETER :: pi = 3.1415926535898D0, RadDeg = 180.0D0 / pi, DegRad = pi / 180.0D0
   COMPLEX (KIND=8), PARAMETER :: i  = ( 0.0D0, 1.0D0 )
   ! 存储pi，弧度转角度，角度转弧度以及虚部算子i

END MODULE MathConstants

