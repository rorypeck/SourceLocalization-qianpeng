x = 0:5000;
xt    = 2.0 * ( x - 1300.0 ) / 1300.0;
C0 = 1500;
c     = C0 * ( 1.0 + 0.00737*( xt - 1.0 + exp( -xt ) ) );
figure;
plot(c, xt)
set(gca, 'yDir', 'reverse')