!**********************************钱鹏的学习笔记***********************************
!******选择从这个程序开始看起，从名字来看，bellhopMod是bellhop中变量的定义module*******
!******************有点类似与c++面向对象的思想，即定义变量和程序分开来*****************
!*********************想要学习这个程序，首先要了解变量代表的含义**********************
!*******************************日期：2023年4月26日*********************************


MODULE bellhopMod

  USE MathConstants
  INTEGER, PARAMETER :: ENVFile = 5, PRTFile = 6, RAYFile = 21, SHDFile = 25, ARRFile = 36, SSPFile = 40, MaxN = 100000
  ! 定义读取和写入文件的通道，定义每条声线计算的最大步长MaxN=100000

  ! Reduce MaxN (= max # of steps along a ray) to reduce storage
  ! Note space is wasted in NumTopBnc, NumBotBnc ...

  INTEGER            :: Nrz_per_range, iStep
  ! 定义每个距离上的接收器个数，定义声线计算的步长索引
  REAL    ( KIND= 8) :: freq, omega, SrcDeclAngle, SrcAzimAngle
  ! 定义频率、角频率、声源掠射角、声源俯仰角
  CHARACTER (LEN=80) :: Title
  ! 定义标题

  ! *** Beam structure ***

  TYPE rxyz
     REAL (KIND=8) :: r, x, y, z
  END TYPE rxyz
  ! 定义rxyz结构体，r代表距离、(x,y,z)分别代表笛卡尔坐标系下的当前声线步长下的声线坐标

  TYPE BeamStructure
     INTEGER           :: NBeams, Nimage, Nsteps, iBeamWindow
     REAL     (KIND=8) :: deltas, epsMultiplier = 1, rLoop
     CHARACTER (LEN=1) :: Component              ! Pressure or displacement
     CHARACTER (LEN=4) :: Type = 'G S '
     CHARACTER (LEN=7) :: RunType
     TYPE( rxyz )      :: Box
  END TYPE BeamStructure
  ! 定义波束结构体，包括声线数、、步长数、波束的窗索引
  ! 定义步长、、距离循环
  ! 定义压力或者位移
  ! 定义类型
  ! 定义运行类型
  ! 定义整个声线计算的场景长宽

  TYPE( BeamStructure ) :: Beam

  ! *** ray structure ***

  TYPE ray2DPt
     INTEGER          :: NumTopBnc, NumBotBnc
     REAL   (KIND=8 ) :: x( 2 ), t( 2 ), p( 2 ), q( 2 ), c, Amp, Phase
     COMPLEX (KIND=8) :: tau
  END TYPE ray2DPt
  TYPE( ray2DPt )     :: ray2D( MaxN )

  ! 定义声线结构体
  ! 定义声线的海面反射次数和海底反射系数
  ! 定义声线的(x,y)坐标，法向向量，，，声速，幅度，相位
  ! 定义到达接收器位置的时延
  ! 定义结构体数组MaxN

  ! uncomment COMPLEX below if using paraxial beams !!!
  TYPE ray3DPt
     REAL    (KIND=8) :: p_tilde( 2 ), q_tilde( 2 ), p_hat( 2 ), q_hat( 2 ), DetQ
     REAL    (KIND=8) :: x( 3 ), t( 3 ), phi, c, Amp, Phase
     INTEGER          :: NumTopBnc, NumBotBnc
     ! COMPLEX (KIND=8) :: p_tilde( 2 ), q_tilde( 2 ), p_hat( 2 ), q_hat( 2 ), f, g, h, DetP, DetQ
     COMPLEX (KIND=8) :: tau

  END TYPE ray3DPt
  TYPE( ray3DPt )     :: ray3D( MaxN )

  ! 三维bellhop声线结构体

END MODULE bellhopMod
