function x_tau = overlap(x , tau, lambda, fs)
% 将信号循环截断
N = size(x,2);
ntau = floor(fs* tau);
x_ex = [x x x];
x_tau = zeros(length(ntau), N);
for it = 1 : length(ntau)
    x_tau(it,:) = lambda(it)* x_ex(N+1- ntau(it) : 2*N- ntau(it));
end

% tj = (1:N)/fs;
% NM = length(ntau);
% im  = (1: NM).';
% x_tau_r = real(x_tau);
% scale = max(max(abs(x_tau_r)));
% pres_t_nor = x_tau_r / scale *(NM-1) / NM...
%     + im * ones(1,N);
% set(0,'defaultfigurecolor','w')
% figure; set(gcf, 'position', [ 100 100 1250 500])
% beaplot(tj, pres_t_nor, '-' ,1.5, '多次脉动', '时间(s)','信号幅度',1,0)
% set(gca,'ytick',im)
% set(gca,'yticklabel',num2str(im))
end