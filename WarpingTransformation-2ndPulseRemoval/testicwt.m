wt = cwt(Wwxt);
xrec = icwt(wt);
figure;
plot(tj_w, Wwxt,'r',tj_w, xrec,'k')
bumpmtlb = cwt(Wwxt,'bump',fs);
xrec = icwt(bumpmtlb,'bump');%,'SignalMean',mean(Wwxt)
figure;
plot(tj_w,Wwxt)
xlabel('Seconds')
ylabel('Amplitude')
hold on
plot(tj_w,xrec,'r')
legend('Original','Reconstruction')