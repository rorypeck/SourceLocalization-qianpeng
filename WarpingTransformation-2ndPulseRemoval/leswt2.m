function [coefs1, f1] = leswt2(fs, z1, wavename, f)
% 定义信号信息


% %旧版本
% wavename='cmor1-3'; %可变参数，分别为cmor的
%举一个频率转尺度的例子
% fmin=2;
% fmax=20;
% df=0.1;

t1 = (1: length(z1))/fs;
% t2 = (1: length(z2))/fs;
% f=fmin:df:fmax-df;%预期的频率
% wcf=centfrq(wavename); %小波的中心频率
% scal=fs*wcf./f;%利用频率转换尺度
[coefs1, f1] = cwt(z1,wavename,fs);
% coefs2 = cwt(z2,scal,wavename);
coefs1dB = 10* log10(abs(coefs1) / max(max(abs(coefs1))));
figure; %set(gcf, 'position', [ 100 100 1200 500])
% tiledlayout(1,2,'TileSpacing','Compact','padding','Compact')
% nexttile;
beapcolor(t1,f1, coefs1dB,'时频分析', '时间(s)', '频率(Hz)' ,'jet', [-30 0], 14, 1);shading interp
% axis([0.05 0.55 0 700])
% nexttile;
% beapcolor(t2,f, abs(coefs2),'(b)存在多次脉动信号时频分析', '时间(s)', '频率(Hz)' ,'jet');shading interp
% axis([0.05 0.55 0 700])
end
% ————————————————
% 版权声明：本文为CSDN博主「hyhhyh21」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
% 原文链接：https://blog.csdn.net/weixin_42943114/article/details/89603208