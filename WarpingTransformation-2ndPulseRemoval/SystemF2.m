function [rr, rd, pres_t, pres_t_nor, fs] = SystemF2(filename, fmin , fmax, tmin,NM)
%该程序为阵列信号仿真第二个程序，用来输出一定带宽的系统函数
tic
fs = 2000;
fst = 0;
T = 2;
N = fs * T;
df = (fs - fst) / N;
dt = 1/ (fs- fst);
tj = tmin + (0:N-1) * dt;
f0 = fst + (0:N-1) * df;
f1 = f0((f0>=fmin));
K = N - length(f1);
f1 = f1(f1<=fmax);
kraken(filename)
[ ~, ~, ~, ~, Pos, ~ ] = read_shd([filename '.shd'],f1(1));
rd = Pos.r.z;
rr = Pos.r.range;
sd = Pos.s.z;
isd = abs(rd - sd)< 1e-3;
pres = zeros(length(rd), N); a = zeros(NM, N);
im = (1: NM).';
for i = 1: length(f1)
    changeenv(f1(i),filename);
    kraken(filename)
    [ ~, ~, ~, ~, ~, pressure ] = read_shd([filename '.shd'],f1(i));
    pres(:,i+K) = squeeze(pressure);
    [ Modes ] = read_modes( filename, f1(i));
    
%     a(:, i+K) = phi.' * pres(:, i+K);% .* phi(ird,im).';
    phi = Modes.phi(:,im); krm = Modes.k(im); rho = Modes.rho;
    a(:, i+K) = 1/(rho*sqrt(8*pi*rr))*exp(-1j*pi/4)*phi(isd,:).*(exp(-1j*krm*rr)./sqrt(krm)).';
end

pres_t = df *2* abs(ifft(a,N,2));%.*(ones(NM,1)* exp(1j*tmin*2*pi*f0))
scale = max(max(abs(pres_t)));
pres_t_nor = pres_t / scale *(NM-1) / NM...
    + im * ones(1,N);
set(0,'defaultfigurecolor','w')
figure; set(gcf, 'position', [ 100 100 1250 500])
% subplot(132)
beaplot(tj, pres_t_nor, '-' ,1.5, '简正波幅度', '时间(s)','简正波号数',1,0)
% xlim([tmin tmin+0.6])
set(gca,'ytick',im)
set(gca,'yticklabel',num2str(im))
toc
end