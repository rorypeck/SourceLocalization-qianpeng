clc; close all;
% 仿真爆炸声源接收信号
filename = 'ideal_v1';
fmin = 100; fmax = 150; tmin = 12.1/1.49-0.15; NM = 4;
[rr, rd, pres_t, pres_t_nor, fs] = SystemF(filename, fmin , fmax, tmin, NM);
saveas(gcf, '.\SystemF\1.png')
saveas(gcf, '.\SystemF\1.fig')
[rr, rd, pres_t, pres_t_nor, fs] = SystemF2(filename, fmin , fmax, tmin, NM);
save('data3.mat','rr','rd','pres_t','pres_t_nor','fs');
load('data3.mat')
tj = tmin + (1: size(pres_t,2))/fs;
im = (1:NM).';
D = 100;
c = 1500;
fc = (im *c)/(2*D);
tr = rr / c;
x = sum(pres_t .* exp( 1j * 2 * pi * fc * sqrt(tj.^2 - tr^2)),1);


lambda = [0.3, 0.2 , 0.1];
tau = [0.05, 0.07, 0.08];
x_tau = overlap(x , tau, lambda, fs);
r = x + sum(x_tau,1);

figure; set(gcf, 'position', [ 100 100 1200 500])
tiledlayout(1,2,'TileSpacing','Compact','padding','Compact')
nexttile;
beaplot(tj, real(x), '-', 1, '(a)无多次脉动接收信号', '时间(s)', '信号幅度', 0, 0)
axis tight
nexttile;
beaplot(tj, real(r), '-', 1, '(b)存在多次脉动接收信号', '时间(s)', '信号幅度', 0, 0)
axis tight
saveas(gcf, '.\脉动接收信号\1.png')
saveas(gcf, '.\脉动接收信号\1.fig')


% % x_in = [real(r) imag(r)];
% [~, ~,~] = funAOK(x, fs , 256, 512, 2, 2);
% caxis([-100,-60])
% xlim([0 1])
% [t_axis, f_axis,ofp] = funAOK(r, fs , 256, 512, 2, 2);
% caxis([-100,-60])
% xlim([0 1])
saveas(gcf, '.\funAOK\1.png')
saveas(gcf, '.\funAOK\1.fig')

wavename='morse';
f = 1: fs/2;
% leswt(fs, real(x), wavename, f);
coefs = leswt(fs, real(x), real(r), wavename, f);
saveas(gcf, '.\leswt\1.png')
saveas(gcf, '.\leswt\1.fig')
% [S,F,T] = stft(x_in,fs);
% beapcolor(T,F, abs(S),...
%     '时频分析', 'Time [s]', 'Frequency [Hz]' ,'jet');

[corrx, lagsx] = xcorr(real(x),'normalized');
[corr, lags] = xcorr(real(r),'normalized');

figure; set(gcf, 'position', [ 100 100 1200 500])
tiledlayout(1,2,'TileSpacing','Compact','padding','Compact')
nexttile;
beaplot(lagsx/fs, corrx, '-', 1, '(a)无多次脉动接收信号自相关函数', '时间(s)', '相关系数', 0, 0)
axis([-0.4 0.4 -0.5 1])
nexttile;
beaplot(lags/fs, corr, '-', 1, '(b)存在多次脉动接收信号自相关函数', '时间(s)', '相关系数', 0, 0)
axis([-0.4 0.4 -0.5 1])
saveas(gcf, '.\xcorr\1.png')
saveas(gcf, '.\xcorr\1.fig')

tj_w = (1:11e3)/fs;
wt = sqrt(tj_w.^2 + tr^2);
Wwxt = sqrt(tj_w./wt) .* interp1(tj, real(r), wt, 'linear','extrap');
figure;
plot(tj_w, Wwxt)
[coefs2, f2] = leswt2(fs, Wwxt, wavename, f);
axis([0 3 1 100])
% saveas(gcf, '.\leswt2\1.png')
% saveas(gcf, '.\leswt2\1.fig')

coefs3 = zeros(size(coefs2));
f_reub = [10 17 24 32];
f_relb = [5 13 20 28];
indexlb = zeros(4,1); indexub = zeros(4,1); 
for i = 1: length(f_reub)
    indexlb(i) = find(f2 >= f_relb(i), 1, 'last');
    indexub(i) = find(f2 <= f_reub(i), 1, 'first');
end
index = [indexub(1): indexlb(1) indexub(2): indexlb(2) indexub(3): indexlb(3) indexub(4): indexlb(4)];
for i = 1: length(index)
    coefs3(index(i), : ) = coefs2(index(i), : );
end
z1 = ileswt2(fs, coefs3, wavename, f2);
axis([0 3 1 100])
figure;
beaplot(tj_w, z1, 'k-', 1, 'warping变换重构时域信号', '时间(s)', '信号幅度', 0, 0)
hold on;
beaplot(tj_w, Wwxt, 'r-', 1, 'warping变换重构时域信号', '时间(s)', '信号幅度', 0, 0)
% saveas(gcf, '.\warpingre\1.png')
% saveas(gcf, '.\warpingre\1.fig')

leswt(fs,Wwxt, z1, wavename, f);
nexttile(1);
title('(a)滤波前时频变换'); axis([0 3 1 100])
nexttile(2);
title('(b)滤波后时频变换'); axis([0 3 1 100])
saveas(gcf, '.\leswt2\3.png')
saveas(gcf, '.\leswt2\3.fig')

indext = find((tj <wt(end)) .* (tj>wt(1)));
tj_c = tj(indext); tj_wc = tj_w(indext);
% t_re = sqrt(wt.^2 - tr.^2);
xt_re =  sqrt(wt./tj_w) .* z1;
xt_re1 = interp1(wt, xt_re, tj_c, 'linear','extrap');
figure;
plot(tj_c, xt_re1)
leswt2(fs, xt_re1, wavename, f);
axis([0 0.6 0 700])
% saveas(gcf, '.\leswt2\4.png')
% saveas(gcf, '.\leswt2\4.fig')

[corrxre, lagsxre] = xcorr(xt_re1,'normalized');

figure; set(gcf, 'position', [ 100 100 1200 500])
tiledlayout(1,2,'TileSpacing','Compact','padding','Compact')
nexttile;
beaplot(lags/fs, corr, '-', 1, '(a)', '时间(s)', '相关系数', 0, 0)
axis([-0.4 0.4 -0.5 1])
nexttile;
beaplot(lagsxre/fs, corrxre, '-', 1, '(b)', '时间(s)', '相关系数', 0, 0)
axis([-0.4 0.4 -0.5 1])
saveas(gcf, '.\xcorr\2.png')
saveas(gcf, '.\xcorr\2.fig')